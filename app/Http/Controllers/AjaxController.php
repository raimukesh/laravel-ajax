<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    public function index(Request $request){
        // dd($request);
        // $msg = $request;
        $msg = Auth::user();
        // dd($msg);
        return response()->json(array('msg'=> $msg), 200);
    }

    public function liveSearch()
    {
        return view('pages.live_search');
    }

    public function action(Request $request)
    {
        // dd($request->all());
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = User::where('name', 'like', '%'.$query.'%')
         ->orWhere('email', 'like', '%'.$query.'%')
         ->orWhere('created_at', 'like', '%'.$query.'%')
         ->orWhere('updated_at', 'like', '%'.$query.'%')
         ->orderBy('name', 'desc')
         ->get();
      }
      else
      {
       $data = User::get();
      }

      $total_row = $data->count();

    //   if($total_row > 0)
    //   {
    //    foreach($data as $row)
    //    {
    //     $output = '
    //     <tr>
    //      <td>'.$row->name.'</td>
    //      <td>'.$row->email.'</td>
    //      <td>'.$row->created_at.'</td>
    //      <td>'.$row->updated_at.'</td>
    //      <td>
    //         <a class="btn btn-sm btn-danger" href="home" >Delete</a>
    //      </td>
    //     </tr>
    //     ';
    //    }
    //   }
    //   else
    //   {
    //    $output = '
    //    <tr>
    //     <td align="center" colspan="5">No Data Found</td>
    //    </tr>
    //    ';
    //   }
      $data1 = array(
       'table_data'  => $data,
       'total_data'  => $total_row
      );
    //   dd($data);

      echo json_encode($data1);
     }
    }

    public function deleteUser($id){
        $user = User::find($id);
        if (isset($user)){
            $user->delete();

            return redirect()->back();
        }
        else {
            echo "Nothing to delete";
        }
        // dd($user);
    }

    public function viewUser($id){
        $user_data = User::find($id);
        // dd($user_data);
        return view('pages.user-profile')->with('user_data', $user_data);
    }
}
