<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = User::orderBy('name', 'ASC')->get();
//        dd($users);
        return view('home')->with('users', $users);
    }

    public function showUserDetail(Request $request){

        $user_detail = User::where('id', $request->id)->orderBy('id', 'DESC')->first();
//        dd($user_detail);
        return ($user_detail);
    }
}
