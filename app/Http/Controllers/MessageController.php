<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(){
//        dd("index");
        return view('pages.pusher.pusher');
    }

    public function getSender(){
        return view('pages.pusher.sender');
    }

    public function postSender(Request $request){

        $text = $request['content'];
//        dd(new MessageSent(($text)));
//        dd(event(new MessageSent($text)),"-",$request->all());
//        dd($text);
        event(new MessageSent($text));

//        dd($request->all());
        return back();
    }



}
