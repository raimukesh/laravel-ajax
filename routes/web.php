<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/user_detail', 'HomeController@showUserDetail')-> name('user_detail');


Route::get('/ajax',function() {
    return view('pages.message');
 });
// Route::post('/delete',['uses' => 'TelephoneController@destroy', 'as' => 'delete']);
Route::post('/getmsg',['uses' => 'AjaxController@index', 'as' => 'getmsg']);

Route::get('/live_search', 'AjaxController@liveSearch')->name('live_search');
Route::get('/live_search/action', 'AjaxController@action')->name('live_search.action');
Route::get('/delete/{id}', 'AjaxController@deleteUser')->name('user.delete');
Route::get('{id}/view_user', 'AjaxController@viewUser')->name('user.view');

Route::group(['prefix' => 'profile', 'as'=>'profile.'], function(){
    Route::get('/profile/view', ['uses' => 'ProfileController@myProfile','as' => 'view']);
});

Route::get('/comment_count', function(){
    return view('pages.jQuery.comment_count');
});
Route::get('/Style', function(){
    return view('pages.jQuery.style_methods');
});

Route::get('/scoreboard', function(){
    return view('pages.jQuery.scoreboard');
});
Route::get('/php_function', function(){
    return view('pages.test.static_function');
});

Route::get('test', function () {
    throw new \Exception('First Exception detected');
});

Route::get('game', function (){
    return view('pages.dinosaur.game');
});

Route::get('pusher','MessageController@index')->name('pusher');
Route::get('sender','MessageController@getSender')->name('sender');
Route::post('sender','MessageController@postSender')->name('sender');

