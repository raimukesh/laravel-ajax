<!DOCTYPE html>
<html>
<head>
	<title>Font Test</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <style>
        body {
            background-image: url("https://s3.amazonaws.com/codecademy-content/courses/jquery/images/typetile.png");
            background-repeat: repeat;
            margin: 0;
            padding: 0;
            }

            .outer-container{
                width: 100%;
            }
            h1 {
                color: #FF6D04;
                font-size: 2rem;
                font-weight: 300;
                letter-spacing: 10px;
                line-height: 1rem;
                text-align: center;
            }

            h2 {
                color: #665C3D;
                font-size: .9rem;
                font-weight: 100;
                letter-spacing: 2px;
                margin-bottom: 20px;
                text-transform: uppercase;
                text-align: center;
            }

            header {
                text-align: center;
                font-family: "verdana";
            }

            .preview {
                align: center;
                background: white;
                border-radius: 20px;
                border-color: #E6B000;
                border-style: solid;
                border-width:10px;

                font-family: Helvetica;
                font-size: 16px;
                font-weight: normal;
                margin: 10px auto 10px auto;

                min-height: 100px;
                padding: 20px;
                word-wrap: break-word;
            }

            textarea {
                border-color: #E6B000;
                border-radius: 10%;
                border-style: solid;
                border-width:20px;
                display: block;
                font-family: "Helvetica-Light", "Helvetica", "Arial", Arial, sans-serif;
                font-size: 14px;
                line-height: 20px;
                margin: 10px auto 10px auto;
                padding: 20px;
            }

            .formHolder {
                background-color: #665C3D;
                border-radius: 50px 50% 50px 50%;
                color: beige;
                font-family: "verdana";
                font-size: 0.75rem;
                margin: auto auto 0 auto;
                padding: 20px;
                text-transform: uppercase;
                height: 20px;
            }

            .form {
                display: inline;
            }

            /* .third {
                height: 100%;
                float: center;
                text-align: center;
            } */

            input {
                display: inline;
            }

            #fonts {
                margin-left: 4px;
            }

            #weight {
            }
    </style>
</head>
<body>
  <div class = "outer-container">
  <header>
    <h1>"Preview Fonts"</h1>
    <h2>Test-drive your text</h2>
  </header>
  <div class="formHolder">
        {{-- <form method="POST"> --}}
            <div style="float: left;">Font Family:
                <select name="font" id="font">
                <option value="helvetica" id="helvetica">Helvetica</option>
                <option value="times" id="times">Times</option>
                <option value="impact" id="impact">Impact</option>
                <option value="courier" id="courier">Courier</option>
                <option value="Segoe UI" id="Segoe UI">Segoe UI</option>
                <option value="Brush Script MT" id="Brush Script MT">Brush Script MT</option>
                <option value="Consolas" id="Consolas">Consolas</option>
                <option value="verdana" id="verdana">Verdana</option>
                </select>
            </div>
            <div style="float: left;">Font Size:
                <input type="text" placeholder="16" id="size">
            </div>
            <div style="float: left;">Font Weight:
                <select name="weight" id="weight">
                <option value="normal">Normal</option>
                <option value="bold">Bold</option>
                <option value="bolder">Bolder</option>
                <option value="lighter">lighter</option>
                </select>
            </div>
        {{-- </form> --}}
  </div>
  <div class="preview" >Sample Font</div>
    <textarea name="text" placeholder="Type something here and see what happens..." id="text" cols="100" rows="10"></textarea>
  </div>
</body>
<script>
    $(document).ready(() => {

        $('#text').on('keyup',(event)=>{
            $('.preview').html($(event.currentTarget).val());
        })
        $('#font').on('change', (event)=>{
                let $fontFamily = $(event.currentTarget).val();
                $('.preview').css('fontFamily', $fontFamily);
            })
        $('select#weight').on('change', (event)=>{
            let $weight = $(event.currentTarget).val();
            $('.preview').css('fontWeight', $weight);
        })
        $('input#size').on('keyup', (event)=>{
            let $size = $(event.currentTarget).val()+'px';
            $('.preview').css('fontSize', $size);
        })
    })
</script>
</html>
