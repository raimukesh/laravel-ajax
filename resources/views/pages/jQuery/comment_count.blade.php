{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html>
<head>
	<title>Comment Count</title>
	<link href="https://s3.amazonaws.com/codecademy-content/projects/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet" type="text/css">
    <style>
        html, body {
            margin: 0;
            padding: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
            }

            body {
            background-color: CornflowerBlue;
            }

            .container {
            max-width: 700px;
            height: 100%;
            margin: 0 auto;
            }

            .header {
            background-color: #19314D;
            }

            .header h1 {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 32px;
            font-weight: 200;
            color: #fff;
            margin: 0;
            padding: 7px 0;
            }

            .nav-menu {
            background-color: #2d4766;
            color: #EFEFEF;
            display: none;
            justify-content: flex-start;
            }
            .nav-menu .container-nav {
                padding: 1.5em;
                display: flex;
                flex-direction: row;
                justify-content: space-around;
            }
                .nav-menu .container-nav ul {
                list-style: none;
                margin: 0;
                padding: 0;
                }
                .nav-menu .container-nav ul li {
                    padding-top: .25em;
                    }
                    .nav-menu .container-nav ul li:first-child {
                    text-transform: uppercase;
                    color: CornflowerBlue;
                    letter-spacing: 0.05em;}

            .post {
            background-color: #fff;
            border-radius: 4px;
            padding: 10px;
            margin: 20px 0;
            box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.3);
            }

            form .post {
                min-height: 186px;
            }

            .post .avatar {
            margin: 0 10px 10px 0;
            }

            .post h3 {
            font-weight: 700;
            font-size: 16px;
            margin-top: 0;
            display: inline;
            }

            .post p {
            font-size: 14px;
            }

            .post p + img {
            width: 100%;
            margin-bottom: 15px;
            }

            .btn {
            color: #00CD73;
            border-color: #00CD73;
            background-color: transparent;
            font-weight: 700;
            }

            .btn-hover {
            color: white;
            border-color: #00CD73;
            background-color: #00CD73;
            font-weight: 700;
            }

            .btn-post{
            color: CornflowerBlue;
            border-color: CornflowerBlue;
            border-radius: 4px;
            border-width: 1px;
            text-decoration: none;
            background-color: transparent;
            font-weight: 700;
            padding: 5px;
            float: right;
            margin-right: 20px;
            }

            .btn-post:hover {
            color: white;
            border-color: CornflowerBlue;
            background-color: CornflowerBlue;
            font-weight: 700;
            }

            .postText {
            width: 100%;
            display: block;
            word-wrap: break-word;
            height: 60px;
            resize: none;
            }

            textarea{
            background-color: #F2F2F2;
            border: none;
            box-shadow: inset 0px 1px 1px 0px rgba(0, 0, 0, 0.1);
            line-height: 20px;
            font-size: 14px;
            padding: 10px;
            }

            .wordcount {
            color: #9B9B9B;
            font-size: 9px;
            line-height: 24px;
            opacity: .8;
            }

            .red {
            color: red;
            }

    </style>
    {{-- <link href="styles.css" rel="stylesheet"> --}}
</head>

 <body>
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-xs-2">
            <h1 ><a href="{{ route('home') }}" style="color:seashell;">Feedster</a></h1>
          </div>
          <div class="col-xs-7">
          </div>

          <div class="col-xs-3">
            <h1 class="menu" style="text-align : center; ">Menu</h1>
          </div>
        </div>
      </div>
    </div>

  <div class='nav-menu'>
    <div class='container-nav'>
      <ul>
        <li>Bang Bang</li>
        <li>Mexican-Chinese</li>
        <li>Indo-Scandinavian</li>
        <li>Polish-Korean</li>
      </ul>
      <ul>
        <li>Contact</li>
        <li>Twitter</li>
        <li>Facebook</li>
        <li>Instagram</li>
      </ul>
    </div>
  </div>

    <form action="javascript:;" method= "POST" class="newPost">
      <div class="container">
        <div class="post">
          <img class="avatar" src="https://s3.amazonaws.com/codecademy-content/courses/jquery+jfiddle+test/feedster/profile-purple.svg">
          <h3 class='name'>COMMENT COUNT</h3>
          <textarea class="postText" placeholder="What's on your mind?"></textarea>
          <p class="wordcount"><span class='characters'>20</span> characters remaining</p>
          <div class="button-holder">
            <button type="submit" class="btn-post">POST</button>
            <button type="clear" class="btn-post">Clear</button>
          </div>
        </div>
      </div>
    </form>

    <div class="posts">
      <div class="container">
        <div class="post">
          <img class="avatar" src="https://s3.amazonaws.com/codecademy-content/courses/jquery+jfiddle+test/feedster/profile-teal.svg">
          <h3>Ivory Breath</h3>
          <p>Buffalo bulgogi, are you kidding me!?! Do yourself a favor and head to this restaurant.</p>
          <button class="btn">+1</button>
        </div>

        <div class="post">
          <img class="avatar" src="https://s3.amazonaws.com/codecademy-content/courses/jquery+jfiddle+test/feedster/profile-yellow.svg">
          <h3>Bacon West</h3>
          <p>Tacos, sausage, fundido, saurkraut and more! 10/10 would recommend.</p>
          <button class="btn">+1</button>
        </div>

      </div>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">
    </script>

  <script>
    $(document).ready(() => {
        // Menu
        $('.menu').on('mouseover', ()=>{
            $('.menu').css('backgroundColor','#2d4766');
            $('.nav-menu').show();
        })
        $('.nav-menu').on('mouseleave', ()=>{
            $('.menu').css('backgroundColor','#19314D');
            $('.nav-menu').hide();
        })
        // +1 button
        $('.btn').on('mouseover', (event)=>{
            $(event.currentTarget).addClass('btn-hover');
        }).on('mouseleave', (event)=>{
            $(event.currentTarget).removeClass('btn-hover');
        })
        // word count
        $('.postText').focus();
        $('.postText').on('keyup',(event)=>{
            let post = $(event.currentTarget).val();
            let remaining = 20 - post.length;
            $('.characters').html(remaining);
            if(remaining <=0){
            $('.wordcount').addClass('red');
            }
            else{
            $('.wordcount').removeClass('red');
            }
        })

    });

  </script>

  </body>

</html>
