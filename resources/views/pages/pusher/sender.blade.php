<!DOCTYPE html>
<html>
<head>
    <title>Send Message</title>
</head>
<body>
<h1>Send Your Message through Pusher</h1>

<div>
    <form action="{!! route('sender') !!}" method="post">
        @csrf
        <label>Your Message:</label><br>
        <textarea type="text" name="content" cols="40" rows="5" placeholder="your message..."></textarea><br>
        <button type="submit" style="height: 50px; width: 100px; background: steelblue; font-size: 16px;">SUBMIT</button>
    </form>

</div>
<div id="message">

</div>


</body>
</html>

