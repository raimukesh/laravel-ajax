<!DOCTYPE html>
<html>
<head>
    <title>Pusher Test</title>
</head>
<body>
<h1>Pusher Test</h1>
<div>
    <h4>Messages:</h4>
    <ol id="message">

    </ol>

</div>
<div class='container'>
    Seconds : <div id='seconds'></div>
</div>

<!-- Script -->
<script type='text/javascript'>

    let count = 0;
    let myInterval;
    // Active
    window.addEventListener('focus', startTimer);

    // Inactive
    window.addEventListener('blur', stopTimer);

    function timerHandler() {
        count++;
        document.getElementById("seconds").innerHTML = count;
    }

    // Start timer
    function startTimer() {
        console.log('focus');
        myInterval = window.setInterval(timerHandler, 1000);
    }

    // Stop timer
    function stopTimer() {
        window.clearInterval(myInterval);
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>
<script>

    // Enable pusher logging - don't include this in production
    // Pusher.logToConsole = true;

    const pusher = new Pusher('c8d958ac90757527c6ce', {
        cluster: 'ap2',
        forceTLS: true
    });

    let channel = pusher.subscribe('my-channel');
    channel.bind('message-sent', function(data) {
        // window.addEventListener('blur',()=>{
        //     alert(JSON.stringify(data));
        // });
        // alert(data.text);
        if(data.msg != null){

            let msg = "<li>"+data.msg+"</li>";
            $('#message').append(msg);
            // Inactive window
            if(document.hasFocus()==false){
                // alert(JSON.stringify(data));
                alert("You have new message.")
            }
        }

    });
</script>

</body>
</html>

