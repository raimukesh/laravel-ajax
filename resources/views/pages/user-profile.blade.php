@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-4 offset-md-4 card" ><hr>
        <h4 style="text-align:center; color:royalblue; ">"{{ $user_data->name }}'s Profile"<hr><hr></h4>
        <div id="user_profile">
            <ul>
                <li>Name : {{$user_data->name}}</li>
                <li>Email : {{$user_data->email}}</li>

                <li>Email Verified at : @if($user_data->email_verified_at) ? {{$user_data->email_verified_at}} @else Not verified @endif</li>

                <li>Join Date : {{ $user_data->created_at}}</li>
                <li>Update Date : {{ $user_data->updated_at }}</li>
            </ul>
            <button class="btn btn-sm btn-outline-danger" id="hide">Hide</button>

        </div>
        <button class="btn btn-sm btn-outline-success" id="show" style="display: none;">Show</button>
        <hr>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function(){
    $("button#hide").click(function(){
      $("div#user_profile").hide(2000);
      $("button#hide").hide(1500);
      $("button#show").show(2000);
    });

    $("button#show").click(function(){
      $("div#user_profile").show(1500);
      $("button#hide").show(1800);
      $("button#show").hide(1500);
    });
  });
</script>

@endsection
