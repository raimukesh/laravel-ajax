<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Static function</title>
</head>
<body>
    <p>
        <span title="Php Static function">< ?php </span><br><br>
        &nbsp&nbsp class a{ <br>

        &nbsp&nbsp static protected $test="class a";<br><br>

        &nbsp&nbsp public function static_test()<br>{ <br>
            &nbsp&nbsp&nbsp&nbspecho static::$test; // Results class b <br>
            &nbsp&nbsp&nbsp&nbspecho self::$test; // Results class a<br>
            &nbsp&nbsp}<br>

            &nbsp&nbsp}<br>
        <br>
        &nbsp&nbsp class b extends a<br>{  <br>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspstatic protected $test="class b";<br>
        }<br><br>

        &nbsp&nbsp&nbsp&nbsp$obj = new b();<br>
        &nbsp&nbsp&nbsp&nbsp$obj->static_test();<br><br>
    ?> <br><br>
    <h4>Result:</h4>

    </p>

    <?php
        class a{

        static protected $test="Result from class a using 'static :: $ test;' ";

        public function static_test(){
            echo static::$test."<br>"; // Results class b
            echo self::$test; // Results class a
        }

        }

        class b extends a{
            static protected $test="Result from 'class b' using 'self :: $ test; '";
        }

        $obj = new b();
        $obj->static_test();
    ?>
</body>
</html>
