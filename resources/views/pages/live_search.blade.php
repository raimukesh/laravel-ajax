@extends('layouts.app')

@section('content')

  <div class="container box">
   <h3 align="center">Live search in laravel using AJAX</h3><br />
   <div class="panel panel-default">

    <div class="panel-heading">Search Customer Data</div>
    <div class="panel-body">
     <div class="form-group">
      <input type="text" name="search" id="search" class="form-control" placeholder="Search Customer Data" />
     </div>

     <div class="table-responsive">
         {{-- Total data count --}}
      <h3 align="center">Total Data : <span id="total_records"></span></h3>

      <table class="table table-striped table-bordered" >
       <thead>
        <tr>
         <th>Name</th>
         <th>Email</th>
         <th>Join Date</th>
         <th>Update Date</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody id="table-data">
           <a id="href" href="#">
            <tr>
                <a href="#"></a>
                {{-- <input type="text" id="user_id"> --}}
                {{-- <td id="name"></td>
                <td id="email"></td>
                <td id="join_date"></td>
                <td id="update_date"></td> --}}
            </tr>
           </a>

       </tbody>
      </table>
      {{-- <ul id="data-list">
      </ul> --}}
     </div>
    </div>
   </div>
  </div>

@endsection

@section('script')
<script>
        $(document).ready(function(){

         fetch_customer_data();

         function fetch_customer_data(query = '')
         {
          $.ajax({
           url:"{{ route('live_search.action') }}",
           method:'GET',
           data:{query:query},
           dataType:'json',
           success:function(data)
           {
            //    console.log(data.table_data);
            $("#table-data").children(["tr","td"]).remove();


            $.each(data.table_data, function () {
                var id = this.id;
                // alert(id);
                var newRowContent = "<tr><td>" + this.name +"</td><td>" + this.email + "</td><td>" + this.created_at + "</td><td>" + this.updated_at + "</td><td><a class='btn btn-sm btn-primary' href='http://localhost/Ajax/"+this.id+"/view_user/' >view</a> <a class='btn btn-sm btn-danger' href='http://localhost/Ajax/delete/"+this.id+"'>Delete</a></td></tr>";
                $('#table-data').append(newRowContent);

                // $("#table-data").append("<input type='text' value=" + this.id + ">");

                // $("#email").html(this.email);
                // $("#join_date").html(this.created_at);
                // $("#update_date").html(this.updated_at);

                console.log(this.name);

            })


                // $('#table-data').append("<td>" + this.email +"</td>");

            $('#total_records').text(data.total_data);
            }
            })
            }
        // })

         $(document).on('keyup', '#search', function(){
          var query = $(this).val();
          fetch_customer_data(query);
         });
        });
</script>

@endsection
