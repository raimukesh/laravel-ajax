@extends('layouts.app')

@section('content')
       <div class="container">
        <div id = 'msg'>
            <p>This message will be replaced using Ajax.
                Click the button to replace the message.</p>
        </div>
            <button class="btn btn-outline-info btn-sm" onclick="getMessage()">Replace Message</button>
       </div>

@endsection

@section('script')

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <script>

      function getMessage() {
        //   alert('hello');
        // console.log('hello');

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

          $.ajax({
             type:'POST',
             url: "{{ route('getmsg') }}",
             headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            //  data:[],
             success:function(data) {
                //  console.log(JSON.stringify(data.msg));
                 alert(JSON.stringify(data.msg));
                $("#msg").html(JSON.stringify(data.msg));
             }
          });
       }
    </script>
   </body>

</html>
@endsection
