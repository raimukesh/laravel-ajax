@extends('layouts.app')

@section('content')
<div class="container">

@if(Auth::user())
<div class="row justify-content-md-center">
    {{-- jQery show/hide --}}
    <div class="col-md-4 card" ><hr>
        <h4 style="text-align:center; ">"My Profile 1"<hr><hr></h4>
        <div id="user_profile1">
            <ul>
                <li>Name : {{Auth::user()->name}}</li>
                <li>Email : {{Auth::user()->email}}</li>

                <li>Email Verified at : @if(Auth::user()->email_verified_at) ? {{Auth::user()->email_verified_at}} @else Not verified @endif</li>

                <li>Join Date : {{ Auth::user()->created_at}}</li>
                <li>Update Date : {{ Auth::user()->updated_at }}</li>
            </ul>
            <button class="btn btn-sm btn-danger" id="hide">Hide</button>

        </div>
        <button class="btn btn-sm btn-dark" id="show" style="display: none;">Show</button>
        <hr>
    </div>
    {{-- jQuery Slide-toggle --}}
    <div class="col-md-4 card" ><hr>
        <h4 id="" style="text-align:center;">"My Profile 2"<hr><hr></h4>
        {{-- <button id="flip" class="btn btn-sm btn-dark">slideToggle</button> --}}
        <div id="user_profile2">
            <ul>
                {{-- <li>User Id : {{Auth::user()->id}}</li> --}}
                <li>Name : {{Auth::user()->name}}</li>
                <li>Email : {{Auth::user()->email}}</li>

                <li>Email Verified at : @if(Auth::user()->email_verified_at) ? {{Auth::user()->email_verified_at}} @else Not verified @endif</li>

                <li>Join Date : {{ Auth::user()->created_at}}</li>
                <li>Update Date : {{ Auth::user()->updated_at }}</li>
                {{-- {{ Auth::user()}} --}}
            </ul>
        </div>
        <button id="flip" class="btn btn-sm btn-primary">Slide Toggle</button>
        <hr>
    </div>
    {{-- jQery fade --}}
    <div class="col-md-4 card" ><hr>
        <h4 style="text-align:center;">"My Profile 3"<hr><hr></h4>
        <div id="user_profile_3">
            <ul>
                <li>Name : {{Auth::user()->name}}</li>
                <li>Email : {{Auth::user()->email}}</li>
                <li>Email Verified at : @if(Auth::user()->email_verified_at) ? {{Auth::user()->email_verified_at}} @else Not verified @endif</li>
                <li>Join Date : {{ Auth::user()->created_at}}</li>
                <li>Update Date : {{ Auth::user()->updated_at }}</li>
            </ul>
        </div>
        <div style="text-align:center;">
            <button class="btn btn-sm btn-default" id="fadeOut" >Fade-out</button>
            <button class="btn btn-sm btn-dark" id="fadeIn" >Fade-in</button>
        </div>
        <hr>
    </div>

    {{-- <div class="w-100"></div> --}}

    {{-- fade-toggle --}}
    <div class="col-md-4 card" ><hr>
        <h4 style="text-align:center;">"My Profile 4"<hr><hr></h4>
        <div id="user_profile_4" style="display:none;">
            <ul>
                <li>Name : {{Auth::user()->name}}</li>
                <li>Email : {{Auth::user()->email}}</li>

                <li>Email Verified at : @if(Auth::user()->email_verified_at) ? {{Auth::user()->email_verified_at}} @else Not verified @endif</li>

                <li>Join Date : {{ Auth::user()->created_at}}</li>
                <li>Update Date : {{ Auth::user()->updated_at }}</li>
            </ul>
        </div>
        <button class="btn btn-sm btn-primary" id="fade_in">Fade-Toggle</button>
        <hr>
    </div>
    {{--  --}}
    <div class="col-md-4 card" ><hr>
        <h4 style="text-align:center;">"My Profile 5"<hr><hr></h4>
        <div id="user_profile_5">
            <ul>
                <li>Name : {{Auth::user()->name}}</li>
                <li>Email : {{Auth::user()->email}}</li>

                <li>Email Verified at : @if(Auth::user()->email_verified_at) ? {{Auth::user()->email_verified_at}} @else Not verified @endif</li>

                <li>Join Date : {{ Auth::user()->created_at}}</li>
                <li>Update Date : {{ Auth::user()->updated_at }}</li>
            </ul>
        </div>
        <button class="btn btn-sm btn-primary" id="">Fade-Toggle</button>
        <hr>
    </div>
    {{--  --}}
    <div class="col-md-4 card" ><hr>
        <h4 style="text-align:center;">"My Profile 6"<hr><hr></h4>
        <div id="user_profile_6">
            <ul>
                <li>Name : {{Auth::user()->name}}</li>
                <li>Email : {{Auth::user()->email}}</li>

                <li>Email Verified at : @if(Auth::user()->email_verified_at) ? {{Auth::user()->email_verified_at}} @else Not verified @endif</li>

                <li>Join Date : {{ Auth::user()->created_at}}</li>
                <li>Update Date : {{ Auth::user()->updated_at }}</li>
            </ul>
        </div>
        <button class="btn btn-sm btn-primary" id="">Fade-Toggle</button>
        <hr>
    </div>

</div>
@endif

</div>

@endsection

@section('script')
<script>
    $(document).ready(function(){
    // Show/hide
    $("button#hide").click(function(){
      $("button#hide").hide(500, function(){
        $("button#show").show(800, function(){
            $("div#user_profile1").hide(1000);
        });
      });
    });
    $("button#show").click(function(){
      $("div#user_profile1").show(1000, function(){
        $("button#show").hide(1000, function(){
            $("button#hide").show(500);
        });
      });
    });

    // slideToggle
    $("#flip").click(function(){
        $("#user_profile2").slideToggle("slow");
    });

    // Fade-in/out
    $("button#fadeOut").click(function(){
        $("div#user_profile_3").fadeTo("slow", 0.1);
        $("button#fadeIn").click(function(){
            $("div#user_profile_3").fadeTo("slow", 1);
        });
    });

    // fadeToggle
    $("button#fade_in").click(function(){
        $("div#user_profile_4").fadeToggle("slow");
    });

  });
</script>

@endsection
