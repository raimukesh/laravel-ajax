
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inspector Dashboard</title>
    <link rel="icon" href="https://app.inspector.dev/images/favicon.png" type="image/png" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="JnXwohdZ40UTXFg7t0CWQ71HZlOtwgkUjNkqwR4p">

    <!-- Social share style -->
    <meta property="og:image" content="https://app.inspector.dev/images/frontend/dashboard.png"/>
    <meta property="og:title" content="Get free real-time monitoring & alerting."/>
    <meta property="og:url" content="https://app.inspector.dev/register"/>
    <meta property="og:description" content="Get instant alerts if your back-end fails and identify issues at a glance before users are aware of them."/>
    <meta property="og:type" content="article"/>

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:image" content="https://app.inspector.dev/images/frontend/dashboard.png"/>
    <meta name="twitter:title" content="Get free real-time monitoring & alerting."/>
    <meta name="twitter:url" content="https://app.inspector.dev/register"/>
    <meta name="twitter:description" content="Get instant alerts if your back-end fails and identify issues at a glance before users are aware of them."/>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="/css/style.css?id=b640de9a345f792f5da9" rel="stylesheet">

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '731890293828657');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=731890293828657&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135189893-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-135189893-1');
    </script>

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        window.Organization = {"id":380,"name":"technology sales","email":"lostinecho2070@gmail.com","address":null,"city":null,"zip_code":null,"country":null,"vat":null,"current_usage":"0","referral_link":"https:\/\/app.inspector.dev\/register?referrer=5de6109410564","referrals_count":0,"created_at":"2019-12-03 07:36:52","updated_at":"2019-12-03 07:36:52"};
    </script>

    <script>
        fbq('track', 'CompleteRegistration');
    </script>

    <!-- Hotjar Tracking Code for https://www.inspector.dev -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1049507,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Customerly live chat -->
    <script>
        window.customerlySettings = {
            app_id: "06efa3b2",
            user_id: "412",// Optional
            name: "Mukesh Rai",
            email: "lostinecho2070@gmail.com",

            //If you manage different user companies you can track the companies and their attributes like this:
            company: {
                company_id: "380",
                name: "technology sales",
            }
        };
        !function(){function e(){var e=t.createElement("script");e.type="text/javascript",e.async=!0,e.src="https://widget.customerly.io/widget/06efa3b2";var r=t.getElementsByTagName("script")[0];r.parentNode.insertBefore(e,r)}var r=window,t=document,n=function(){n.c(arguments)};r.customerly_queue=[],n.c=function(e){r.customerly_queue.push(e)},r.customerly=n,r.attachEvent?r.attachEvent("onload",e):r.addEventListener("load",e,!1)}();
    </script>
</head>
<body style="height: 100vh">
<div id="app" style="height: 100%">

    <App :user="{&quot;id&quot;:412,&quot;name&quot;:&quot;Mukesh Rai&quot;,&quot;email&quot;:&quot;lostinecho2070@gmail.com&quot;,&quot;email_verified_at&quot;:&quot;2019-12-03 07:36:52&quot;,&quot;is_org_admin&quot;:true,&quot;created_at&quot;:&quot;2019-12-03 07:36:52&quot;,&quot;updated_at&quot;:&quot;2019-12-03 07:36:52&quot;,&quot;deleted_at&quot;:null,&quot;organization_id&quot;:380,&quot;organization&quot;:{&quot;id&quot;:380,&quot;name&quot;:&quot;technology sales&quot;,&quot;email&quot;:&quot;lostinecho2070@gmail.com&quot;,&quot;address&quot;:null,&quot;city&quot;:null,&quot;zip_code&quot;:null,&quot;country&quot;:null,&quot;vat&quot;:null,&quot;created_at&quot;:&quot;2019-12-03 07:36:52&quot;,&quot;updated_at&quot;:&quot;2019-12-03 07:36:52&quot;,&quot;deleted_at&quot;:null,&quot;stripe_id&quot;:null,&quot;card_brand&quot;:null,&quot;card_last_four&quot;:null,&quot;trial_ends_at&quot;:null,&quot;locked_at&quot;:null,&quot;referral_code&quot;:&quot;5de6109410564&quot;,&quot;referred_by&quot;:null,&quot;subscriptions&quot;:[]}}">
        <!--placeholder to show while App component is loading on page for the first time-->
        <VSpinner/>
    </App>

</div>

<!-- Scripts -->
<script src="/js/manifest.js?id=89712bc6d8c588400cae"></script>
<script src="/js/vendor.js?id=00a21643656954df75c4"></script>
<script src="/js/app.js?id=dbae03547245f8f0054a" defer></script>
</body>
</html>
