@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="row col-md-12">
                <div class="card col-md-8">
                    <div class="card-header">User Details from Controller</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->updated_at }}</td>
                                    <td>
                                        <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card col-md-4">
                    <div class="card-header">User Details with Ajax(POST)</div>
                    <div class="card-body">
                        {{--                        Select user--}}
                        <div class="form-group">
                            <label>Select User Name:</label>

                            <select class="form-control" name="user" id="user">
                                <option>-- SELECT USER --</option>
                                @if (!empty($users))
                                    @foreach ($users as $data)
                                        <option value={{ $data->id }}>{{$data->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        {{--List user Detail--}}

                        <div class="form-group"  id="userDetail" style="display: none;">
                        <form method="" action="">
                            <div id="append">
                                    <label class="control-label">User Details</label>
                                        <ul>
                                        {{-- User Detail is listed here using js/Ajax --}}
                                        </ul>

                            </div>
                            <a href="" onclick="alert('Thank You'); return false; " class="btn btn-sm btn-primary">Update</a>
                        </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('public/js/jquery.min.js')}}"></script>

    <script>
        $(document).ready(function (){
            $("#user").on ('change',  function(){
                //fetches selected "user id" and stores in "user_id"
                let user_id = $('#user').val();
                // console.log(user_id);

                // removes previous data from list-view
                $("#append").children("li").remove();

                //retrieving assigned user from task id.
                $.ajax({
                    type: "post",
                    url: "{{ route('user_detail') }}",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },

                    data: "id=" + user_id,

                    // gets data from getAssignedUserByTaskId() in var 'msg'
                    success: function (user_detail) {
                        // console.log(user_detail);
                        $('#userDetail').show();

                        $("#append").append("<li>Name:" + user_detail.name +"</li>");
                        $("#append").append("<li>Email:&nbsp" + user_detail.email +"</li>");
                        $("#append").append("<li>Join Date : " + user_detail.created_at +"</li>");
                        $("#append").append("<li>Update Date : " + user_detail.updated_at +"</li>");
                        $("#append").append("<li>Email Verification Date : " + user_detail.email_verified_at +"</li>");


                    }
                });
            });

        });
    </script>
@endsection
