<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel::Particle.js</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        {{-- particles styles --}}
        <link rel="stylesheet" media="screen" href="{{ asset('css/particles/style.css') }}">

        <!-- Styles -->
        <style>
            /* html, body {*/
            /*     background-color: #111;*/
            /*    color: #636b6f;*/
            /*    font-family: 'Nunito', sans-serif;*/
            /*    font-weight: 200;*/
            /*    height: 100vh;*/
            /*    margin: 0;*/
            /*}*/

            /*.full-height {*/
            /*    height: 100vh;*/
            /*}*/

            /*.flex-center {*/
            /*    align-items: center;*/
            /*    display: flex;*/
            /*    justify-content: center;*/
            /*}*/

            /*.position-ref {*/
            /*    position: relative;*/
            /*}*/

            /*.top-right {*/
            /*    position: absolute;*/
            /*    right: 10px;*/
            /*    top: 18px;*/
            /*}*/

            /*!* .content {*/
            /*    text-align: center;*/
            /*} *!*/

            /*.title {*/
            /*    font-size: 84px;*/
            /*}*/

            /*.links > a {*/
            /*    color: #636b6f;*/
            /*    padding: 0 25px;*/
            /*    font-size: 13px;*/
            /*    font-weight: 600;*/
            /*    letter-spacing: .1rem;*/
            /*    text-decoration: none;*/
            /*    text-transform: uppercase;*/
            /*}*/

            /*.m-b-md {*/
            /*    margin-bottom: 30px;*/
            /*}*/
             canvas{
                 display:block;
                 vertical-align:bottom;
             }

             .full-height {
                 height: 100vh;
             }

             .flex-center {
                 align-items: center;
                 display: flex;
                 justify-content: center;
             }

             .position-ref {
                 position: relative;
             }

             .top-right {
                 position: absolute;
                 right: 10px;
                 top: 18px;
             }

             .links > a {
                 color: #636b6f;
                 padding: 0 25px;
                 font-size: 13px;
                 font-weight: 600;
                 letter-spacing: .1rem;
                 text-decoration: none;
                 text-transform: uppercase;
                 background:#fffdfd;
             }

             /* ---- particles.js container ---- */

             #particles-js{
                 width: 100%;
                 height: 50%;
                 background-color: #1b1616;
                 background-image: url("{{ asset('images/darkG.jpg')  }}");
                     /*url('http://localhost/Ajax/public/images/darkG.jpg');*/
                 background-size: cover;
                 background-position: 50% 50%;
                 background-repeat: no-repeat;
             }

        </style>

    </head>
    <body>
        <div class="flex-centerd position-ref full-height">
            @if (Route::has('login'))
                <div>
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/home') }}">Home</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register W</a>
                            @endif
                        @endauth
                    </div>
                </div>
            @endif

            <div class="content">
                <div id="particles-js"></div>
            </div>
        </div>

        <!-- scripts -->
        <script src="{{ asset('public/js/particles/particles.js')}}"></script>
        <script src="{{ asset('public/js/particles/app.js') }} "></script>

        {{-- <script>
            <script src="particles.js"></script>

            /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
            particlesJS.load('particles-js', 'assets/particles.json', function() {
            console.log('callback - particles.js config loaded');
            });
        </script> --}}
    </body>
</html>
